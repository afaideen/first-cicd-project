# First CICD Project

Note: It is highly recommended to use docker command under DOS environment to quickstart running the demo app. 
- Open docker desktop and make sure it is running.
- Open win command terminal (DOS) cd under dir first-cicd-project
- Build docker image
> docker build -t first-cicd-app:v1.0 .

- Run the image built, 
> docker run -p 8080:8080  first-cicd-app:v1.0



