#FROM python:3.8.0-slim
FROM python:3.9-slim
WORKDIR /app
ADD . /app
#RUN pip install --trusted-host pypi.python.org Flask
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
ENV VAR_NAME han
#CMD ["python", "app.py"]
#CMD ["python", "app_employee.py"]
# Start the Flask application using Gunicorn
#CMD ["gunicorn", "--bind", "0.0.0.0:8080", "app_emplyee:app"]
#CMD ["gunicorn -b 0.0.0.0:8080 -w 4 ", "app_employee.py"]
#gunicorn -b 0.0.0.0:8000 -w 4 myapp.app:app
#CMD ["gunicorn","app_employee:app","--host","0.0.0.0","--port","8080"]
#CMD gunicorn app:app --bind 0.0.0.0:$PORT --reload
#CMD gunicorn app_employee:app --bind 0.0.0.0:8080 --reload
CMD gunicorn app_employee:app --bind 0.0.0.0:8080 --reload -w 4
